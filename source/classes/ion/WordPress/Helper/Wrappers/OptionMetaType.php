<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\WordPress\Helper\Wrappers;

/**
 * Description of OptionType
 *
 * @author Justus
 */

class OptionMetaType {
    
    public const POST = 1;
    public const TERM = 2;
    public const USER = 3;
    public const COMMENT = 4;
}
